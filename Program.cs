﻿using System;

namespace HomeWork2v1
{
    class Program
    {
        static void center(string text)
        {
            // Функция center
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine(text);
        }
        static void Main(string[] args)
        {
            

            
            // Заказчик просит написать программу «Записная книжка». Оплата фиксированная - $ 120.

            // В данной программе, должна быть возможность изменения значений нескольких переменных для того,
            // чтобы персонифецировать вывод данных, под конкретного пользователя.

            // Для этого нужно: 
            // 1. Создать несколько переменных разных типов, в которых могут храниться данные
            //    - имя;
            //    - возраст;
            //    - рост;
            //    - баллы по трем предметам: история, математика, русский язык;

            // 2. Реализовать в системе автоматический подсчёт среднего балла по трем предметам, 
            //    указанным в пункте 1.

            // 3. Реализовать возможность печатки информации на консоли при помощи 
            //    - обычного вывода;
            //    - форматированного вывода;
            //    - использования интерполяции строк;

            // 4. Весь код должен быть откомментирован с использованием обычных и хml-комментариев

            // **
            // 5. В качестве бонусной части, за дополнительную оплату $50, заказчик просит реализовать 
            //    возможность вывода данных в центре консоли.

            // Составное форматирование
            String FirstName = "Sasha"; // Имя ученика
            String LastName = "Pashkovski"; // Фамилия ученика
            int Age = 26; // Возраст ученика
            double Height = 188.5; // Рост ученика
            int M = 5; // оценка по Математике
            int H = 3; // оценка по Истории
            int R = 3; // оценка по Русскому языку
            double S = ((double)M + (double)H + (double)R) / 3; // Средний балл по трем предметам
            Console.WriteLine(S);
            /// <summary>
            /// Новая переменая,где средний балл имеет 1 цифру после запятой
            /// </summary>
            String SFormated = S.ToString("#.#");
           
            string pattern = "Имя: {0} \nФамилия:{1} \nВозраст: {2} \nРост: {3} \nМатематика: {4} \nИстория: {5} \nРусский язык: {6} \nСредний балл {7}"; // pattern для форматированного вывода
            
            // обычный вывод
            Console.WriteLine("Имя:" + FirstName);
            Console.WriteLine("Фамилия:" + LastName);
            Console.WriteLine("Возраст:" + Age);
            Console.WriteLine("Рост:" + Height);
            Console.WriteLine("Математика:" + M);
            Console.WriteLine("История:" + H);
            Console.WriteLine("Русский язык:" + R);
            Console.WriteLine("Средний балл:" + SFormated);
            //форматированный вывод
            
            Console.WriteLine(pattern,
                              FirstName, 
                              LastName, 
                              Age, 
                              Height, 
                              M, 
                              H, 
                              R, 
                              SFormated);
            //Интерполяция строк
            Console.WriteLine($"Имя: {FirstName} \nФамилия: {LastName} \nВозраст: {Age} \nРост: {Height} \nМатематика: {M} \nИстория: {H} \nРусский язык: {R} \nСредний балл: {SFormated}");
            //задание с **

            // обычный вывод
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine("Имя:" + FirstName);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine("Фамилия:" + LastName);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine("Возраст:" + Age);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine("Рост:" + Height);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine("Математика:" + M);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine("История:" + H);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine("Русский язык:" + R);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine("Средний балл:" + SFormated);

            //форматированный вывод
            string pattern1 = "Имя: {0}";
            string pattern2 = "Фамилия: {0}";
            string pattern3 = "Возраст: {0}";
            string pattern4 = "Рост: {0}";
            string pattern5 = "Математика: {0}";
            string pattern6 = "История: {0}";
            string pattern7 = "Русский язык: {0}";
            string pattern8 = "Средний балл: {0}";

            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine(pattern1,
                              FirstName);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine(pattern2,
                              LastName);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine(pattern3,
                              Age);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine(pattern4,
                              Height);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine(pattern5,
                              M);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine(pattern6,
                              H);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine(pattern7,
                              R);
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine(pattern8,
                              SFormated);
            //интерполяция строк
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine($"Имя: {FirstName}");
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine($"Фамилия: {LastName}");
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine($"Возраст: {Age}");
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine($"Рост: {Height}");
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine($"Математика: {M}");
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine($"История: {H}");
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine($"Русский язык: {R}");
            Console.CursorLeft = (Console.WindowWidth - 10) / 2;
            Console.WriteLine($"Средний балл: {SFormated}");
            //при помощи F center
            center("Имя:" + FirstName);
            center("Фамилия:" + LastName);
            center("Возраст:" + Age);
            center("Рост:" + Height);
            center("Математика:" + M);
            center("История:" + H);
            center("Русский язык:" + R);
            center("Средний балл:" + SFormated);

            Console.ReadKey();
        }
    }
}
